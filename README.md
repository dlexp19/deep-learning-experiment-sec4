# deep-learning-experiment-sec4

## Experiment

### Mnist / MLP

$ python3 train_mnist_mlp.py

To test the trained model, run

$ python3 test_mnist_mlp.py -m <path to model> -i <path to image>

### Mnist / CNN

$ python3 train_mnist_cnn.py

### CIFAR-10 / CNN

$ python3 train_cifar10.py -d <path to dataset>

To test the trained model, run

$ python3 test_cifar10.py -m <path to model> -d <path to dataset>

### Feature extraction

$ python3 create_db.py -d <path to dataset directory>

$ python3 search.py -i <path to source image>
